const https = require('https')

const HOST_NAME = 'gitlab.com'
const GITLAB_PROJECT_ID = '32820089'
const GITLAB_ACCESS_TOKEN = 'glpat-ojRxHQde4KF3sAU6FVNg'
const GITLAB_API_PATH = `/api/v4/projects/${GITLAB_PROJECT_ID}`

const baseOptions = {
  hostname: HOST_NAME,
  path: GITLAB_API_PATH,
  headers: {
    'private-token': GITLAB_ACCESS_TOKEN,
    'Content-Type': 'application/json'
  }
}

async function apiClient(newOptions = { method: 'GET' } , data) {
  const options = { ...baseOptions, newOptions }
  return new Promise((resolve, reject) => {
    let result = {data: []}
    const req = https.request(options, (res)=> {
      res.on('data', (d) => {
        result.data.push(d)
      })
      res.on('end', () => {
        let buffer = Buffer.concat(result.data)
        resolve(JSON.stringify(buffer.toString()))
      })
    })
    req.on('error', reject)
    // if(options.method !== 'GET') {
    //   req.write(JSON.stringify(data))
    // }

    req.end()
  })
}

function getVersion() {
  const { version } = require('../package.json')
  return version
}

async function main () {
  const version = getVersion()
  // console.log(version) // 1.0.0
  try {
    const resp = await apiClient({method: 'GET'})
    if(resp.statusCode !== 200) {
      throw 'error to get list'
    }
      // console.log(resp)
  }
  catch (e) {
    console.error(e)
  }  
}
main()